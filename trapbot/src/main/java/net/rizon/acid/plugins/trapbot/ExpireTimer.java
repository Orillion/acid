package net.rizon.acid.plugins.trapbot;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import net.rizon.acid.core.Timer;

class ExpireTimer extends Timer
{
	ExpireTimer()
	{
		// one day
		super(60 * 60 * 24, true);
	}

	@Override
	public void run(Date now)
	{
		for (Iterator<Map.Entry<String, TrappedUser>> it = trapbot.users.entrySet().iterator(); it.hasNext();)
		{
			Map.Entry<String, TrappedUser> e = it.next();
			TrappedUser t = e.getValue();

			/* how many days have passed */
			int days = (int)((now.getTime() - t.lastTrap.getTime()) / (1000 * 60 * 60 * 24));

			if (days >= trapbot.conf.expire)
				it.remove();
		}
	}
}