package net.rizon.acid.plugins.trapbot;

import java.util.Date;

import net.rizon.acid.core.Message;
import net.rizon.acid.core.Protocol;
import net.rizon.acid.core.Timer;

class RetrapTimer extends Timer
{
	RetrapTimer(long time_from_now, boolean repeating)
	{
		super(time_from_now, repeating);
	}

	/*
	 * release users
	 */
	@Override
	public void run(final Date now)
	{
		/* start enforcing again and announce it */
		trapbot.enforce = true;
		trapbot.retrapTimer = null;

		Protocol.privmsg(trapbot.trapbot.getUID(), trapbot.getTrapChanName(), Message.BOLD + "HAHA TRAPPED AGAIN!" + Message.BOLD);

		trapbot.releaseTimer = new ReleaseTimer();
		trapbot.releaseTimer.start();
	}
}
