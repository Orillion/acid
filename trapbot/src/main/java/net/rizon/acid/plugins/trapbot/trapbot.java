package net.rizon.acid.plugins.trapbot;

import java.util.HashMap;

import net.rizon.acid.core.AcidCore;
import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Event;
import net.rizon.acid.core.Logger;
import net.rizon.acid.core.Plugin;
import net.rizon.acid.core.Timer;
import net.rizon.acid.core.User;
import net.rizon.acid.plugins.trapbot.conf.Config;

public class trapbot extends Plugin
{
	protected static final Logger log = Logger.getLogger(trapbot.class.getName());

	private Event ev;
	public static AcidUser trapbot;
	public static Config conf;
	private Timer expireTimer;
	public static Timer retrapTimer;
	public static Timer releaseTimer;
	// ip -> user
	public static HashMap<String, TrappedUser> users = new HashMap<String, TrappedUser>();
	public static boolean enforce = true;

	public static String getTrapChanName()
	{
		return Acidictive.conf.getChannelNamed(conf.trapchan);
	}

	public static TrappedUser makeTrappedUser(User u)
	{
		TrappedUser tu = new TrappedUser(u.getIP());
		users.put(u.getIP(), tu);
		return tu;
	}

	public static TrappedUser getTrappedUser(User u)
	{
		if (u.getIP() == null || u.getIP().equals("0"))
			return null;

		return users.get(u.getIP());
	}

	public static void freeUser(User u)
	{
		users.remove(u.getIP());
	}

	public static void updateTrap(User u)
	{
		if (u.getServer() == AcidCore.me || u.getServer().isUlined())
			return;

		/* ignore spoofed users */
		if (u.getIP() == null || u.getIP().equals("0") || u.getIP().equals("255.255.255.255"))
			return;

		TrappedUser t = getTrappedUser(u);

		if (t == null)
			t = makeTrappedUser(u);

		t.update();
	}

	@Override
	public void start() throws Exception
	{
		ev = new TrapEvent();

		reload();

		expireTimer = new ExpireTimer();
		expireTimer.start();

		releaseTimer = new ReleaseTimer();
		releaseTimer.start();
	}

	@Override
	public void reload() throws Exception
	{
		conf = (Config) net.rizon.acid.conf.Config.load("trapbot.yml", Config.class);
		Acidictive.loadClients(this, conf.clients);

		User u = User.findUser(conf.trapbot);
		if (u == null || !(u instanceof AcidUser))
			throw new Exception("Unable to find trapbot " + conf.trapbot);

		trapbot = (AcidUser) u;
	}

	@Override
	public void stop()
	{
		ev.remove();

		if (expireTimer != null)
			expireTimer.stop();

		if (releaseTimer != null)
			releaseTimer.stop();

		if (retrapTimer != null)
			retrapTimer.stop();
	}
}
