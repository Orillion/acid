import traceback
import logging
import task
import importlib

from core import *

class AcidPlugin(object):
	def __init__(self):
		super(AcidPlugin, self).__init__()

		self.prefix = config.get('control').get('prefix')
		self.logchan = config.get('control').get('channel')
		self.log = logging.getLogger(__name__)
		self.config = config
		self.dbp = dbx.cursor() # do these have to be closed?

	def start(self):
		pass

	def stop(self):
		pass

	def getCommands(self):
		pass	

modules = {}
plugin_to_modname = lambda x: '%s.%s' % (x, x)

def loadPlugin(plugin):
	global modules

	modname = plugin_to_modname(plugin)

	mod = importlib.import_module(modname)
	obj = getattr(mod, plugin)()

	obj.start()

	modules[plugin] = obj

def unloadPlugin(plugin):
	global modules

	obj = modules[plugin]
	obj.stop()

	deps = [module for module in sys.modules if module.startswith(plugin + '.')]
	for dep in deps:
		del sys.modules[dep]

	del modules[plugin]

def getPlugins():
	return modules.keys()

def call(name, *args):
	task.Run()

	for m in modules.itervalues():
		try:
			func = getattr(m, name)
		except:
			continue

		try:
			func(*args)
		except:
			traceback.print_exc()
			raise
