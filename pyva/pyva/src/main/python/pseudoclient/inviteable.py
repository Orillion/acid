#
# inviteable.py
# Copyright (c) 2014 Kristina Brooks
#
# 
#

from pseudoclient.cmd_manager import *
import mythreading as threading
from utils import strip_ascii_irc
from core import anope_major

import pyva_net_rizon_acid_core_User as User
import pyva_net_rizon_acid_core_Channel as Channel

#---------------------------------------------------------------------#
# this is terrible 

def private_channel_request(self, manager, opts, arg, channel, sender, userinfo):
	if self.auth.is_channel_eligible(arg, sender):
		self.auth.request(sender, arg, 'request')

def private_channel_remove(self, manager, opts, arg, channel, sender, userinfo):
	if not self.channels.is_valid(arg):
		self.notice(channel, "I'm not in @b%s@b." % arg)
	else:
		self.auth.request(sender, arg, 'remove')

def private_help(self, manager, opts, arg, channel, sender, userinfo):
	command = arg.lower()

	if command == '':
		message = manager.get_help()
	else:
		message = manager.get_help(command)

		if message == None:
			message = ['%s is not a valid command.' % arg]

	for line in message:
		self.notice(channel, line)

class PrivateCommandManager(CommandManager):
	def get_prefix(self):
		return ''

	def get_invalid(self):
		return 'Invalid message. Say help for a list of valid messages.'

	def get_commands(self):
		return {
			'request': (private_channel_request, ARG_YES|ARG_OFFLINE, 'requests a channel (must be founder)', [], '#channel'),
			'remove': (private_channel_remove, ARG_YES|ARG_OFFLINE, 'removes a channel (must be founder)', [], '#channel'),
			'hi': 'help',
			'hello': 'help',
			'help': (private_help, ARG_OPT, 'displays help text', []),
		}

#---------------------------------------------------------------------#

class InviteablePseudoclient(object):
	def __init__(self):
		pass

	def start(self):
		self.commands_private = PrivateCommandManager()

	# override this if you want to add additional crap 
	# (ie check if the chan is eligible or not)
	def do_accept(self, nick, channel):
		self.auth.accept(nick)

	def join(self, channel):
		me = User.findUser(self.nick)
		me.joinChan(channel)

	def part(self, channel):
		me = User.findUser(self.nick)
		chan = Channel.findChannel(channel)
		if chan:
			me.partChan(chan)

	def onNotice(self, source, target, message):
		if not self.initialized:
			return

		me = User.findUser(self.nick)
		user = User.findUser(target)
		userinfo = User.findUser(source)

		if me != user or (userinfo != None and userinfo['nick'] != 'ChanServ'):
			return

		try:
			msg = message.strip()
		except:
			return

		if msg.startswith('[#'): #It's a channel welcome message. Let's ignore it.
			return

		self.elog.chanserv('%s' % msg)
		sp = msg.split(' ')

		if anope_major == 2 and msg == 'Password authentication required for that command.':
			self.elog.error('%s is not authed with services (maybe a wrong NickServ password was configured?).' % target)

		# common to both anope1 and anope2
		if "isn't registered" in msg:
			self.auth.reject_not_registered(strip_ascii_irc(sp[1]))
			return

		if len(sp) < 6:
			return

		if 'inviting' in sp[2]: #It's an invite notice. Let's ignore it.
			return

		nick = strip_ascii_irc(sp[0])
		channel = sp[5][:-1]

		if anope_major == 1:
			should_accept = 'Founder' in sp[2]
		elif anope_major == 2:
			channel = strip_ascii_irc(channel)
			should_accept = ('founder' == sp[3])

		if should_accept:
			self.do_accept(nick, channel)
		else:
			self.auth.reject_not_founder(nick, channel)

	def onServerNotice(self, source, target, message):
		if not self.initialized:
			return

		me = User.findUser(self.nick)
		user = User.findUser(target)

		if me == user and 'tried to kick you from' in message.strip():
			sp = message.strip().split(' ')
			nick = strip_ascii_irc(sp[1])
			channel = strip_ascii_irc(sp[7])
			#self.notice(nick, 'To remove this bot (must be channel founder): @b/msg %s remove %s@b' % (self.nick, channel))
			self.auth.request(nick, channel, 'remove')

	def onPrivmsg(self, source, target, message):
		if not self.initialized:
			return False

		userinfo = User.findUser(source)
		myself = User.findUser(self.nick)

		sender = userinfo['nick']
		channel = target
		msg = message.strip()
		index = msg.find(' ')

		if index == -1:
			command = msg
			argument = ''
		else:
			command = msg[:index]
			argument = msg[index + 1:]

		# do we have a deferal command handler?
		if hasattr(self, 'execute'):
			if channel == myself['nick'] and command.startswith(self.commands_private.prefix): # XXX uid
				self.elog.debug('Deferred parsing of private message (sender: @b%s@b, command: @b%s@b, argument: @b%s@b)' % (sender, command, argument))
				threading.deferToThread(self.execute, self.commands_private, command, argument, sender, sender, userinfo)
			elif self.channels.is_valid(channel) and command.startswith(self.commands_user.prefix):
				self.elog.debug('Deferred parsing of channel message (sender: @b%s@b, channel: @b%s@b, command: @b%s@b, argument: @b%s@b)' % (sender, channel, command, argument))
				threading.deferToThread(self.execute, self.commands_user, command, argument, channel, sender, userinfo)
		elif target == self.nick:
			cmd = self.commands_private.get_command(command)
			if cmd == None:
				# not found, forward through
				return True
			#     (self, manager,               opts,    arg,     channel, sender, userinfo):
			cmd[0](self, self.commands_private, command, argument, sender, sender, userinfo)
		else:
			# forward anyway
			return True

	def onInvite(self, inviter, invitee, channel):
		if not self.initialized:
			return False

		me = User.findUser(self.nick)
		if me != invitee:
			return

		if self.auth.is_channel_eligible(channel.getName(), inviter.getNick()):
			self.auth.request(inviter.getNick(), channel.getName(), 'request')
