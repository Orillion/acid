from pseudoclient.sys_auth import AuthManager

class QuotesAuthManager(AuthManager):
	def __init__(self, module):
		AuthManager.__init__(self, module)

	def onAccept(self, user, request, action, channel):
		if action == 'news':
			if self.module.channels.is_valid(channel):
				self.module.channels.set(channel, 'news', True)
				self.module.msg(user, 'Enabled news in @b%s@b.' % channel)
		elif action == 'nonews':
			if self.module.channels.is_valid(channel):
				self.module.channels.set(channel, 'news', False)
				self.module.msg(user, 'Disabled news in @b%s@b.' % channel)
		elif action.startswith('delete_quote'):
			if self.module.channels.is_valid(channel) and action[12:].isdigit():
				self.module.delete_quote(channel, user, int(action[12:]))
		else:
			return False

		return True
