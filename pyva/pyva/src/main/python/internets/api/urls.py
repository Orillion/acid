import urllib
import httplib
from feed import get_json, FeedError

class Urls(object):
	def __init__(self, bitly_user, bitly_key):
		self.bitly_user = bitly_user
		self.bitly_key = bitly_key
	
	def shorten(self, long_url):
		url = 'http://api.bitly.com/v3/shorten?login=%s&apiKey=%s&format=json&' % (self.bitly_user, self.bitly_key)
		url += urllib.urlencode({'longUrl': long_url})
		reply = get_json(url)
		return reply
	
	def expand(self, short_url):
		url = 'http://api.longurl.org/v2/expand?'
		url += urllib.urlencode({
				'url': short_url,
#				'all-redirects': 1,
				'content-type': 1,
				'title': 1,
				'format': 'json'
			})
		try:
			reply = get_json(url)
			errmsg = reply['messages'][0]['message']
			return {'error': errmsg}
		except KeyError as ke:
			return reply
		except FeedError as fe:
			if fe.code == 400:
				url = 'http://longurlplease.appspot.com/api/v1.1?'
				url += urllib.urlencode({
						'q': short_url})
				reply = get_json(url)
				result = reply.get(short_url, None)
				if not result:
					return {'error': 'invalid shortened URL'}
				else:
					return {'long-url': result, 'content-type': 'N/A'}
			elif fe.code == 504:
				return {'error': 'connection timed out'}
