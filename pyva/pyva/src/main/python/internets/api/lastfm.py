import urllib
from feed import get_json

class LastFm(object):
	def __init__(self, key):
		self.api_key = key
	
	def get_data(self, method, **kwargs):
		url = 'http://ws.audioscrobbler.com/2.0/?method=%s&api_key=%s&format=json&' % (method, self.api_key)
		url += urllib.urlencode(kwargs)
		return get_json(url)
		
	def get_user(self, username):
		return self.get_data('user.getinfo', user=username)
	
	def get_recent_tracks(self, username, limit=5):
		return self.get_data('user.getrecenttracks', user=username, limit=limit)
	
	def get_top_tracks(self, username, limit=5):
		return self.get_data('user.gettoptracks', user=username, limit=limit)
	
	def get_top_artists(self, username, limit=5):
		return self.get_data('user.gettopartists', user=username, limit=limit)
