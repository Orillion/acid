import urllib
from feed import XmlFeed

class IpInfo(object):
	def __init__(self, api_key):
		self.api_key = api_key
		cache = {} # todo
		
	def get_info(self, target):
		url = 'http://api.ipinfodb.com/v3/ip-city/?key=%s&format=xml&' % self.api_key
		url += urllib.urlencode({'ip': target})
		reply = XmlFeed(url)
		r = reply.elements('/Response')[0]
		return {
			'ip_addr': r.text('ipAddress'),
			'status': r.text('statusMessage'),
			'country_code': r.text('countryCode', 'N/A'),
			'country_name': r.text('countryName', 'N/A').capitalize(),
			'region': r.text('regionName', 'N/A').capitalize(),
			'city': r.text('cityName', 'N/A').capitalize(),
			'zip': r.text('zipCode', 'N/A'),
			'latitude': r.text('latitude'),
			'longitude': r.text('longitude'),
			'timezone': r.text('timezone', 'N/A')
			}
