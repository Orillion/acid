import utils
from feed import get_json
from time import time

def get_region_from_map(r):
	map = Map().get()
	for entry in map:
		if entry['regionId'] == r['id']:
			return entry

def get_region_from_regions(r):
	regions = Regions().get()
	for entry in regions:
		if entry['id'] == r['id']:
			return entry

def get_country_by_id(id):
	countries = Countries().get()
	for entry in countries:
		if entry['id'] == id:
			return entry


class Map(object):
	def __init__(self):
		self.cache = [0, None]

	def get(self):
		if (time() - self.cache[0]) > 1800:
			data = get_json('http://e-sim.org/apiMap.html')
			self.cache = [time(), data]
			return data
		else:
			return self.cache[1]

class Regions(object):
	def __init__(self):
		self.cache = [0, None]

	def get(self):
		if (time() - self.cache[0]) > 1800:
			data = get_json('http://e-sim.org/apiRegions.html')
			self.cache = [time(), data]
			return data
		else:
			return self.cache[1]

class Countries(object):
	def __init__(self):
		self.data = None
		self.get()
	
	def get(self):
		if not self.data:
			self.data = get_json('http://e-sim.org/apiCountries.html')
			return self.data
		else:
			return self.data
