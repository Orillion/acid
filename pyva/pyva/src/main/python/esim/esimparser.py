from api import utils
from copy import copy
from decimal import Decimal, InvalidOperation
from optparse import *

def check_positive_decimal(option, opt, value):
	try:
		d = Decimal(value)
	except InvalidOperation:
		raise OptionValueError('option %s: invalid decimal value: %s' % (opt, value))

	if d >= 0 and d <= 1000000:
		return d
	else:
		raise OptionValueError('option %s: expected value (0 <= x <= 1000000), got %s instead' % (opt, d))

def check_positive_integer(option, opt, value):
	try:
		i = int(value)
	except ValueError:
		raise OptionValueError('option %s: invalid integer value: %s' % (opt, value))

	if i >= 0:
		return i
	else:
		raise OptionValueError('option %s: expected positive value, got %d instead' % (opt, i))

def check_rank(option, opt, value):
	try:
		return utils.get_rank(value)
	except:
		raise OptionValueError('option %s: invalid rank: %s' % (opt, value))

def check_military_unit_bonus(option, opt, value):
	try:
		i = int(value)
	except ValueError:
		raise OptionValueError('option %s: invalid integer value: %s' % (opt, value))
	
	if i not in [5, 10, 15, 20]:
		raise OptionValueError('option %s: invalid MU bonus percentage: %s' % (opt, value))
	else:
		return i

def check_defense_system_bonus(option, opt, value):
	try:
		i = int(value)
	except ValueError:
		raise OptionValueError('option %s: invalid integer value: %s' % (opt, value))

	if i < 1 or i > 5:
		raise OptionValueError('option %s: invalid defense system level: %s' % (opt, value))
	else:
		return i

class ESimParserOption(Option):
	TYPES = Option.TYPES + ('+decimal', '+integer', 'rank', 'mu_bonus', 'ds_bonus')
	TYPE_CHECKER = copy(Option.TYPE_CHECKER)
	TYPE_CHECKER['+decimal'] = check_positive_decimal
	TYPE_CHECKER['+integer'] = check_positive_integer
	TYPE_CHECKER['rank']     = check_rank
	TYPE_CHECKER['mu_bonus'] = check_military_unit_bonus
	TYPE_CHECKER['ds_bonus'] = check_defense_system_bonus

class ESimParserError(Exception):
	def __init__(self, message):
		self.msg = message

	def __str__(self):
		return str(self.msg)

class ESimParser(OptionParser):
	def error(self, error):
		raise ESimParserError(error)
