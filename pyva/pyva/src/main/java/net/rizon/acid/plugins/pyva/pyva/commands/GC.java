package net.rizon.acid.plugins.pyva.pyva.commands;

import static net.rizon.acid.core.Acidictive.reply;
import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Command;
import net.rizon.acid.core.User;
import net.rizon.acid.plugins.pyva.pyva.pyva;

public class GC extends Command
{
	public GC()
	{
		super(0, 0);
	}

	@Override
	public void Run(User x, AcidUser to, Channel c, final String[] args)
	{
		reply(x, to, c, "Memory: " + (Runtime.getRuntime().freeMemory() / 1024) + " Kb free, " + (Runtime.getRuntime().totalMemory() / 1024) + " Kb total, " + (Runtime.getRuntime().maxMemory() / 1024) + " Kb max.");
		int count = pyva.pyva.gc();
		reply(x, to, c, "Pyva GC objects ready: " + count);
		System.gc();
		reply(x, to, c, "Final: Memory: " + (Runtime.getRuntime().freeMemory() / 1024) + " Kb free, " + (Runtime.getRuntime().totalMemory() / 1024) + " Kb total, " + (Runtime.getRuntime().maxMemory() / 1024) + " Kb max.");
	}
}
