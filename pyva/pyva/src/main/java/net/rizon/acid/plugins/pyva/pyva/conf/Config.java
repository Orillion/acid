package net.rizon.acid.plugins.pyva.pyva.conf;

import java.util.List;

import net.rizon.acid.conf.Client;
import net.rizon.acid.conf.ConfigException;
import net.rizon.acid.conf.Configuration;

public class Config extends Configuration
{
	public List<Client> clients;
	public List<String> plugins;
	public List<String> path;

	@Override
	public void validate() throws ConfigException
	{
	}
}
