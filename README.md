# Rizon Acid Services
Acid is a Java based pseudo IRC server used by [Rizon](https://www.rizon.net) to run bots like Internets, LimitServ, and Quotes, as well as various others.

## Requirements
Required to build are:
* Python2.7, with development headers.
* Maven.
* JDK 1.7 or higher.
* gcc or equivalent C++ compiler with C++11 support for pyva.

## Installing
It is recommended that you run Acid on a Linux OS.

Start by cloning the repo using:
```
> git clone https://gitlab.com/rizon/acid.git
```

And then grab the submodules using:
```
> cd acid
> git submodule init
> git submodule update
```

Go to the folder ```pyva/libpyva/pyva-native/``` and copy ```make.example.properties``` to ```make.properties```. Then modify the values to match your system.

Go back to the acid main folder and run ```mvn package```.

After pyva has been built, symlink ```libpyva.so``` from the pyva-native folder to the acid main folder.
```
> ln -s pyva/libpyva/pyva-native/libpyva.so libpyva.so
```

To obtain the correct python package you can use pip in the ```pyva``` folder
```
> pip install -r requirements.txt
```

## Running
Acid has several configuration files that are needed for the program to successfully start. All configuration files must be put in the acid main folder.

The list of configuration files:
* acidictive.yml (acidictive.example.yml located in ```acid``` folder)
* config.yml (config.example.yml located in ```pyva``` folder)
* pyva.yml (pyva.example.yml located in ```pyva``` folder)

### Database
Install the ```acidcore.sql```, located in ```acid``` database in your MySQL server.

Add yourself to the ```access``` table in the acidcore SQL database. Where ```flags``` matches one of the presets in ```acidictive.yml``` or your own set of privileges (SRA or 1 gives you full access in the default setting).

### Anope configuration
Have the names of the bots you are going to use (ex. Internets) registered and allow them access to ```chanserv/why``` (Anope 1.8) or ```chanserv/status``` (Anope 2.0) command.
LimitServ requires ```chanserv/op``` access.

### Linux
Before you can start Acid with libpyva you need to set the LD_PRELOAD environment variable to libpython, you can either add this to your ```~/.bashrc``` or to the ```run.sh``` script.

LD_PRELOAD should point to libpython, for example:
```
export LD_PRELOAD=/usr/lib64/libpython2.7.so
```

### Windows
Requires a .symlink from libpyva.pyd to pyva.dll for some of the exception features to work.
