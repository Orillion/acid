package net.rizon.acid.conf;

import java.util.List;

import net.rizon.acid.core.Acidictive;

public class Command implements Validatable
{
	public String name, privilege;
	public List<String> channels;
	public String clazz;

	public boolean allowsChannel(String channel)
	{
		if (channels != null)
			for (String s : channels)
				if (Acidictive.conf.getChannelNamed(s).equalsIgnoreCase(channel))
					return true;
		return false;
	}

	@Override
	public void validate() throws ConfigException
	{
		Validator.validateNotEmpty("name", name);
	}
}