package net.rizon.acid.messages;

import java.util.logging.Level;

import net.rizon.acid.core.AcidCore;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.Protocol;
import net.rizon.acid.core.User;

public class Whois extends Message
{
	public Whois()
	{
		super("WHOIS");
	}

	// :4SSAAAAXT WHOIS Acidictive :Acidictive

	@Override
	public void onUser(User whoiser, String[] params)
	{
		User u = User.findUser(params[0]);

		if (u == null)
		{
			AcidCore.log.log(Level.WARNING, "Whois for nonexistent nick " + params[0]);
			return;
		}
		else if (u.getServer() != AcidCore.me)
		{
			AcidCore.log.log(Level.WARNING, "Fake direction for whois (" + u.getServer().getName() + ")");
			return;
		}

		/* :l2thorn.com 311 wild Acidictive service rizon.net * :Flood Monitor
		 * :l2thorn.com 319 wild Acidictive :#debug
		 * :l2thorn.com 312 wild Acidictive geo.rizon.net :Rizon Services
		 * :l2thorn.com 313 wild Acidictive :is an IRC Operator - Server Administrator
		 * :l2thorn.com 307 wild wild :has identified for this nick
		 * :l2thorn.com 671 wild Acidictive :is using a secure connection
		 * :l2thorn.com 310 wild Acidictive :is using modes +Saiopx authflags: [none]
		 * :l2thorn.com 338 wild Acidictive :is actually service@rizon.net [255.255.255.255]
		 * :l2thorn.com 317 wild wild 27 1235833454 :seconds idle, signon time
		 * :l2thorn.com 318 wild Acidictive :End of /WHOIS list.
		 */

		Protocol.numeric(311, whoiser.getUID() + " " + u.getNick() + " " + u.getUser() + " " + u.getVhost() + " * :" + u.getRealName());
		if (whoiser.hasMode("o"))
			Protocol.numeric(312, whoiser.getUID() + " " + u.getNick() + " " + u.getServer().getName() + " :" + u.getServer().getDescription());
		else
			Protocol.numeric(312, whoiser.getUID() + " " + u.getNick() + " *.rizon.net :Where are you?");

		String oper = whoiser.getUID() + " " + u.getNick() + " :is an IRC Operator";
		if (u.hasMode("U"))
			Protocol.numeric(313, oper + " - Network Service");
		else if (u.hasMode("N"))
			Protocol.numeric(313, oper + " - Network Administrator");
		else if (u.hasMode("a"))
			Protocol.numeric(313, oper + " - Server Administrator");
		else if (u.hasMode("o"))
			Protocol.numeric(313, oper);
		if (u.hasMode("S"))
			Protocol.numeric(671, whoiser.getUID() + " " + u.getNick() + " :is using a secure connection");
		if (whoiser.hasMode("o"))
		{
			Protocol.numeric(310, whoiser.getUID() + " " + u.getNick() + " :is using modes +" + u.getModes() + " authflags: $=^_><|");
			Protocol.numeric(338, whoiser.getUID() + " " + u.getNick() + " :is actually " + u.getUser() + "@" + u.getHost() + " [" +
					((u.getIP().equals("0") || u.getIP().equals("255.255.255.255")) ? "255.255.255.255" : u.getIP()) + "]");
		}
		Protocol.numeric(317, whoiser.getUID() + " " + u.getNick() + " " + Acidictive.getUptimeTS() + " " + (Acidictive.startTime / 1000) + " :seconds idle, signon time");
		Protocol.numeric(318, whoiser.getUID() + " " + u.getNick() + " :End of /WHOIS list.");
	}
}