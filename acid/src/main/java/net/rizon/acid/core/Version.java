package net.rizon.acid.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.logging.Level;

public class Version
{
	private static final Logger log = Logger.getLogger(Version.class.getName());
	private static HashMap<String, String> values = new HashMap<String, String>();

	public static void load()
	{
		try
		{
			String[] cmd = { "/bin/sh", "-c", "git --no-pager log -n 1 | grep \"^[a-zA-Z]\"" };
			InputStream is = Runtime.getRuntime().exec(cmd).getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			for (String line; (line = br.readLine()) != null;)
			{
				line = line.trim();
				line = line.replaceAll(":", "");
				while (line.indexOf("  ") != -1)
					line = line.replaceAll("  ", " ");

				int sp = line.indexOf(' ');
				if (sp == -1)
					return;

				String name = line.substring(0, sp);
				String value = line.substring(sp + 1);

				values.put(name, value);
			}

			br.close();
			is.close();

			String[] cmd2 = { "/bin/sh", "-c", "git --no-pager log | grep \"^commit\" | head -n1" };
			is = Runtime.getRuntime().exec(cmd2).getInputStream();
			br = new BufferedReader(new InputStreamReader(is));

			for (String line; (line = br.readLine()) != null;)
				values.put("revision", line);

			br.close();
			is.close();
		}
		catch (IOException ex)
		{
			log.log(Level.INFO, "Unable to load GIT version data", ex);
		}
	}

	public static final String getAttribute(final String attribute)
	{
		return values.get(attribute);
	}

	public static final String getFullVersion()
	{
		String lastRev = getAttribute("revision");
		String lastDate = getAttribute("Date");
		String lastAuthor = getAttribute("Author");

		if (lastRev == null || lastDate == null || lastAuthor == null)
			return "Revision: Unknown";

		String fullVersion = "Revision " + lastRev + " from " + lastDate + " by " + lastAuthor;
		fullVersion = fullVersion.trim();

		return fullVersion;
	}
}
