package net.rizon.acid.core;

import java.util.logging.Level;

public class Protocol
{
	private static final Logger log = Logger.getLogger(Protocol.class.getName());

	private static void privmsgInternal(final String sender, final String recipient, String message)
	{
		AcidCore.sendln(":" + sender + " PRIVMSG " + recipient + " :" + message);
	}

	private static void noticeInternal(final String sender, final String recipient, String message)
	{
		AcidCore.sendln(":" + sender + " NOTICE " + recipient + " :" + message);
	}

	private static void encap(final String server, final String command, final String buffer)
	{
		AcidCore.sendln(":" + AcidCore.me.getSID() + " ENCAP " + server + " " + command + " " + buffer);
	}

	public static void privmsg(final String source, final String recipient, String msg)
	{
		String[] split = msg.split("\n");
		for (String s : split)
			do
			{
				int message_length = 450;
				boolean long_message = s.length() > message_length;
				String sub = s.substring(0, long_message ? message_length : s.length());
				privmsgInternal(source, recipient, sub);
				s = s.substring(long_message ? message_length : s.length());
			}
			while (s.isEmpty() == false);
	}

	public static void notice(final String source, final String recipient, String msg)
	{
		String[] split = msg.split("\n");
		for (String s : split)
			do
			{
				int message_length = 450;
				boolean long_message = s.length() > message_length;
				String sub = s.substring(0, long_message ? message_length : s.length());
				noticeInternal(source, recipient, sub);
				s = s.substring(long_message ? message_length : s.length());
			}
			while (s.isEmpty() == false);
	}

	public static void join(User u, Channel c)
	{
		if (u.getServer() != AcidCore.me)
		{
			log.log(Level.WARNING, "Fake direction for JOIN " + u.getServer().getName() + " <-> " + AcidCore.me.getName());
			return;
		}

		join(u, "", c);
	}

	public static void join(User u, final String status, Channel c)
	{
		if (u.getServer() != AcidCore.me)
		{
			log.log(Level.WARNING, "Fake direction for SJOIN " + u.getServer().getName() + " <-> " + AcidCore.me.getName());
			return;
		}

		AcidCore.sendln(":" + AcidCore.me.getSID() + " SJOIN " + AcidCore.getTS() + " " + c.getName() + " +" + c.getModes(true) + " :" + status + u.getUID());
	}

	public static void part(User u, final String channel)
	{
		if (u.getServer() != AcidCore.me)
		{
			log.log(Level.WARNING, "Fake direction for PART " + u.getServer().getName() + " <-> " + AcidCore.me.getName());
			return;
		}

		AcidCore.sendln(":" + u.getUID() + " PART " + channel);
	}

	public static void quit(User u, final String reason)
	{
		if (u.getServer() != AcidCore.me)
		{
			log.log(Level.WARNING, "Fake direction for QUIT " + u.getServer().getName() + " <-> " + AcidCore.me.getName());
			return;
		}

		AcidCore.sendln(":" + u.getUID() + " QUIT :" + reason);
	}

	public static void kick(User u, final String channel, final String reason)
	{
		AcidCore.sendln(":" + AcidCore.me.getSID() + " KICK " + channel + " " + u.getUID() + " :" + reason);
	}

	public static void kick(AcidUser from, User u, final String channel, final String reason)
	{
		AcidCore.sendln(":" + from.getUID() + " KICK " + channel + " " + u.getUID() + " :" + reason);
	}

	public static void kill(final String target, final String reason)
	{
		// Add kill path or the reason gets broken
		AcidCore.sendln(":" + AcidCore.me.getSID() + " KILL " + target + " :" + AcidCore.me.getName() + " (" + reason + ")");
	}

	public static void svskill(final String target)
	{
		encap("*", "SVSKILL", target);
	}

	public static void noop(Server s)
	{
		encap(s.getName(), "SVSNOOP", "+");
	}

	public static void unnoop(Server s)
	{
		encap(s.getName(), "SVSNOOP", "-");
	}

	public static void capture(User u)
	{
		AcidCore.sendln(":" + AcidCore.me.getSID() + " CAPTURE " + u.getUID());
	}

	public static void uncapture(User u)
	{
		AcidCore.sendln(":" + AcidCore.me.getSID() + " UNCAPTURE " + u.getUID());
	}

	public static void svsmode(User u, final String modes)
	{
		encap("*", "SVSMODE", u.getUID() + " " + u.getNickTS() + " " + modes);
	}

	public static void chgident(User u, final String host)
	{
		encap("*", "CHGIDENT", u.getUID() + " " + host);
	}

	public static void chghost(User u, final String host)
	{
		encap("*", "CHGHOST", u.getUID() + " " + host);
	}

	public static void svsnick(User u, final String newnick)
	{
		encap("*", "SVSNICK", u.getUID() + " " + u.getNickTS() + " " + newnick + " " + AcidCore.getTS());
	}

	public static void svsjoin(User u, final String channel)
	{
		encap("*", "SVSJOIN", u.getUID() + " " + channel);
	}

	public static void svspart(User u, final String channel)
	{
		encap("*", "SVSPART", u.getUID() + " " + channel);
	}

	public static void resv(String source, Server s, final String what, final String reason)
	{
		AcidCore.sendln(":" + source + " RESV " + (s != null ? s.getName() : "*") + " " + what + " :" + reason);
	}

	public static void unresv(String source, final String what)
	{
		AcidCore.sendln(":" + source + " UNRESV * " + what);
	}

	public static void wallop(final String source, final String what)
	{
		AcidCore.sendln(":" + source + " OPERWALL :" + what);
	}

	public static void kline(final String source, int time, final String user, final String host, final String reason)
	{
		if (host.replaceAll("[*.?]", "").isEmpty())
			return;
		AcidCore.sendln(":" + source + " KLINE * " + time + " " + user + " " + host + " :" + reason);
	}

	public static void unkline(final String source, final String user, final String host)
	{
		AcidCore.sendln(":" + source + " UNKLINE * " + user + " " + host);
	}

	public static void mode(final String source, final String target, final String modes)
	{
		AcidCore.sendln(":" + source + " MODE " + target + " " + modes);
	}

	public static void uid(User user)
	{
		// :99h UID deso 1 1233151144 +Saiowy deso canuck 0 99hAAAAAB 0 canuck :canuck
		// :99h UID <nick> <hops> <ts> +<umodes> <user> <vhost> <ip> <uid> <svsts> <realhost> :<geco>
		AcidCore.sendln(":" + AcidCore.me.getSID() + " UID " + user.getNick() + " 1 " + AcidCore.getTS() + " +" + user.getModes() + " " + user.getUser() + " " + user.getVhost() + " 0 " + user.getUID() + " 0 " + user.getHost() + " :" + user.getRealName());
	}

	public static void sid(Server server)
	{
		// :geo.rizon.net SID irc.test.net 2 98C :JUPED
		AcidCore.sendln(":" + AcidCore.me.getSID() + " SID " + server.getName() + " " + (server.getHops() + 1) + " " + server.getSID() + " :" + server.getDescription());
	}

	public static void squit(Server server, final String reason)
	{
		AcidCore.sendln(":" + AcidCore.me.getSID() + " SQUIT " + server.getName() + " :" + reason);
	}

	public static void eob()
	{
		AcidCore.sendln(":" + AcidCore.me.getSID() + " EOB");
	}

	public static void numeric(int num, final String what)
	{
		AcidCore.sendln(":" + AcidCore.me.getSID() + " " + num + " " + what);
	}

	public static void authflags(User target, String flags)
	{
		encap(target.getServer().getName(), "AUTHFLAGS", target.getUID() + " " + flags);
	}

	public static void chgclass(User target, String clazz)
	{
		encap(target.getServer().getName(), "CHGCLASS", target.getUID() + " " + clazz);
	}

	public static void chgrealhost(User target, String sockhost, String realhost)
	{
		encap(target.getServer().getName(), "CHGREALHOST", target.getUID() + " " + sockhost + " " + realhost);
	}

	public static void swebirc(String flag, String uid, String host, String ip, String password, String name, String fakeSockHost, String fakeHost)
	{
		if (flag.equalsIgnoreCase("REQ") || flag.equalsIgnoreCase("ACK") || flag.equalsIgnoreCase("NAK"))
		{
			encap("*", "SWEBIRC", flag.toUpperCase() + " " + uid + " " + host + " " + ip + " * * " + password + " " + name + " " + fakeSockHost + " :" + fakeHost);
		}
	}
}
