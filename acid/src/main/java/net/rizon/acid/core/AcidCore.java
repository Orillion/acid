package net.rizon.acid.core;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;

import net.rizon.acid.util.TrustingSSLSocketFactory;

public abstract class AcidCore
{
	public static final Logger log = Logger.getLogger(AcidCore.class.getName());
	public static Server me;
	public static boolean ssl;
	public static String uplink, password;
	public static int port;
	public static Socket sock;
	public static BufferedReader in;
	public static PrintWriter out;

	public static void start(String server, int port, String name, String description, String password, String SID, boolean ssl)
	{
		me = new Server(name, null, description, 0, SID);

		AcidCore.uplink = server;
		AcidCore.password = password;
		AcidCore.port = port;
		AcidCore.ssl = ssl;
	}

	public static void run() throws IOException, UnknownHostException
	{
		if (ssl)
			sock = new TrustingSSLSocketFactory().createSocket(AcidCore.uplink, port);
		else
			sock = new Socket(AcidCore.uplink, port);

		in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
		out = new PrintWriter(sock.getOutputStream(), true);

		try
		{
			sendln("PASS " + password + " TS 6 :" + me.getSID());
			sendln("CAPAB :TS6 EX IE HUB KLN GLN ENCAP EOB TBURST");
			sendln("SERVER " + me.getName() + " 1 :" + me.getDescription());
			sendln("SVINFO 6 5 0 :" + getTS());

			Acidictive.onStart();

			for (String str; (str = in.readLine()) != null;)
			{
				try
				{
					if (Acidictive.conf.protocol_debug)
					{
						BufferedWriter b = new BufferedWriter(new FileWriter("acid.protocol.debug", true));
						b.write(System.currentTimeMillis() +  " " + str + "\n");
						b.close();
					}

					Timer.processTimers();

					log.log(Level.FINEST, "<-: " + str);

					User user = null;
					Server server = null;

					String source, message_name;
					String[] params;

					try
					{
						String[] tokens = str.split(" ");
						if (tokens.length < 2)
							continue;

						source = null;
						int begin = 0;
						if (tokens[begin].startsWith(":"))
						{
							source = tokens[begin++].substring(1);
							server = Server.findServer(source);
							if (server == null)
								user = User.findUser(source);
						}

						message_name = tokens[begin++];

						int end = begin;
						for (; end < tokens.length; ++end)
							if (tokens[end].startsWith(":"))
								break;
						if (end == tokens.length)
							--end;

						params = new String[end - begin + 1];
						int buffer_count = 0;

						for (int i = begin; i < end; ++i)
							params[buffer_count++] = tokens[i];

						if (params.length > 0)
							params[buffer_count] = tokens[end].startsWith(":") ? tokens[end].substring(1) : tokens[end];
						for (int i = end + 1; i < tokens.length; ++i)
							params[buffer_count] += " " + tokens[i];

						//System.out.println("  Source: " + source);
						//System.out.println("  Message: " + message_name);
						//for (int i = 0; i < params.length; ++i)
						//	System.out.println("    " + i + ": " + params[i]);
					}
					catch (Exception ex)
					{
						log.log(Level.SEVERE, "Error parsing message: " + str, ex);
						continue;
					}

					Message m = Message.findMessage(message_name);
					if (m == null)
					{
						log.log(Level.FINE, "Unknown message " + message_name);
						continue;
					}

					if (user != null)
						m.onUser(user, params);
					else if (server != null)
						m.onServer(server, params);

					m.on(source, params);
				}
				catch (Exception e)
				{
					log.log(Level.SEVERE, "Error processing message: " + str, e);
				}
			}
		}
		catch (SocketException e)
		{
			log.log(Level.SEVERE, "Connection to the server died", e);
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, "Critial uncaught main loop error", e);
		}
		finally
		{
			sock.close();
		}
	}

	public static void sendln(final String line)
	{
		out.println(line);
		log.log(Level.FINEST, "-> " + line);
	}

	public static int getTS()
	{
		return (int) (System.currentTimeMillis() / 1000);
	}

	public static void killNick(String nick, String reason)
	{
		Protocol.kill(nick, reason);

		User x = User.findUser(nick);
		if (x != null)
		{
			x.onQuit();
		}
	}

	public static String arrayFormat(String[] sA, int start, int end)
	{
		StringBuffer str = new StringBuffer();
		if (end >= sA.length)
			return null;
		for (int i = start; i <= end; i++)
		{
			if (str.length() > 0)
				str.append(" ");
			str.append(sA[i]);
		}
		return str.toString();
	}




}
